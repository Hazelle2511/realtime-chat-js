// import * from "./index.js";

const socket = io()
    // const momente = moment()

let username = localStorage.getItem('pseudo')
const messages = document.getElementById('messages')
const form = document.getElementById('form')
const input = document.getElementById('input')
    //Update the list of users
const list = document.getElementById('users')

//Avatar
// const inputAvatar = document.getElementById('form avatar')

const getAvatar = () => {
    const size = Math.floor(Math.random() * 100) + 25;
    const img = document.createElement("img")
    img.innerHTML = "";
    img.src = `url(https://www.placecage.com/${size}/${size})`;
    const src = document.getElementById("avatar");
    const image = src.appendChild(img);
    // return `url(https://www.placecage.com/${size}/${size})`
    return `${image}`;
}

console.log(getAvatar())
    //End of Avatar
    // Demande du pseudo utilisateur (si pas dans le localstorage)
while (!username) {
    username = prompt('Quel est votre pseudo')
    localStorage.setItem('pseudo', username)
}


// Envoi du nouveau user
socket.emit('newUser', username)

// Envoi d'un message
form.addEventListener('submit', function(e) {
    e.preventDefault()
    if (input.value) {
        const msg = input.value;
        // const newdate = new Date()
        // const date = function() {
        //         return `${ newdate.getHours()}:${newdate.getMinutes()}`
        //     }
        // console.log(date())
        const date = moment().format('LLL');

        // const date = new Date().getTime();
        // const date = formatTimeAgo()
        socket.emit('newMessage', username, msg, date, getAvatar())
        input.value = ''
    }
})

// Récupération des nouveaux messages
socket.on('newMessage', function(username, msg, date) {
    var item = document.createElement('li')
    item.textContent = username + ': ' + msg + ' - ' + date
    messages.appendChild(item)
    window.scrollTo(0, document.body.scrollHeight)
})

//JM
socket.on('output-messages', data => {
    console.log(data);

    if (data.length) {
        data.forEach(message => {
            // appendMessages(message.msg)

            const li = document.createElement('li')
            li.textContent = message.username

            li.textContent += message.msg
            li.textContent += message.createdAt
            messages.appendChild(li)
            window.scrollTo(0, document.body.scrollHeight)
        })
    }


})

// Récupération des nouvelles notification
socket.on('newNotification', function(msg) {
    var item = document.createElement('li')
    item.classList = 'notif'
    item.textContent = msg
    messages.appendChild(item)
    window.scrollTo(0, document.body.scrollHeight)
})

// Récupération de la liste des utilisateurs
socket.on('updateUsersList', function(users) {

    // Mettre à jour la liste dans le DOM (document) => Page html
    console.log(users, 'Users list');
    // // var usersList = document.getElementById("users");
    // var html = "";
    // for (var i = user.username; i <= users.length; i++) {
    //     html += `<li>user ${i}<\li>`
    // }
    // usersList.innerHTML = html;

    // On cherche le user dans la liste pour le retirer
    // users = users.filter(user => {
    //     return user.id != userId;
    // });

    //users foreach
    // users.forEach(user in users) {
    //     document.getElementById(users).innerHTML = users
    // }


    users.forEach(user => {
        const li = document.createElement('li')
        li.textContent = user.username
        list.appendChild(li)
    })


})