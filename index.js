const express = require('express')


const Msg = require('./models/messages')
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

//Mongodb connect
const mongoose = require('mongoose');
// const mongoDB = 'mongodb+srv://chatapplication:chatapplication@cluster0.6xjuc.mongodb.net/<message-database>?retryWrites=true&w=majority'
// const mongoDB = 'mongodb+srv://chat_application:chat@cluster0.bbv2c.mongodb.net/messages-database?retryWrites=true&w=majority'
const mongoDB = 'mongodb+srv://chat:chat@cluster0.y56gj.mongodb.net/message_databases?retryWrites=true&w=majority'
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true }).then(() => {
    console.log('connected')
}).catch(err => console.log(err))

//     //time
// const moment = require('moment');
// const date = moment().startOf('hour').fromNow();

// console.log(date);
// const dateTime = require("simple-datetime-formater");

//
// const MongoClient = require('mongodb').MongoClient;

// const url = 'mongodb://127.0.0.1:27017';

// const dbName = 'game-of-thrones';
// let db;

// MongoClient.connect(url, { useNewUrlParser: true }, (err, client) => {
//     if (err) return console.log(err);

//     // Storing a reference to the database so you can use it later
//     db = client.db(dbName);
//     console.log(`Connected MongoDB: ${url}`);
//     console.log(`Database: ${dbName}`)
// })

//
let users = [];
const PORT = 3005

// On sert le dossier plublic/
app.use(express.static('public'));

// /*********************************MULTER***************************************************** */
// const multer = require('multer');
// const path = require('path')
// const helpers = require('./helpers');
// //define storage for the images
// const storage = multer.diskStorage({
//     //destination for files
//     destination: function(request, file, callback) {
//         callback(null, './public/uploads/images')
//     },

//     // add back the extension
//     filename: function(request, file, callback) {
//         callback(null, Date.now() + file.originalname)

//     }
// });

// //upload  parameters for multer
// // const upload = multer({
// //     storage: storage,
// //     limits: {
// //         fieldSize: 1024 * 1024 * 3,
// //     }
// // })


// app.post('/upload-profile', (req, res) => {
//     // 'profile_pic' is the name of our file input field in the HTML form
//     let upload = multer({ storage: storage, fileFilter: helpers.imageFilter }).single('image');

//     upload(req, res, function(err) {
//         // req.file contains information of uploaded file
//         // req.body contains information of text fields, if there were any

//         if (req.fileValidationError) {
//             return res.send(req.fileValidationError);
//         } else if (!req.file) {
//             return res.send('Please select an image to upload');
//         } else if (err instanceof multer.MulterError) {
//             return res.send(err);
//         } else if (err) {
//             return res.send(err);
//         }

//         // Display uploaded image for user validation
//         res.send(`You have uploaded this image: <hr/><img src="${req.file.path}" width="500"><hr /><a href="./">Upload another image</a>`);
//     });
// });

// /*********************************END OF MULTER***************************************************** */
// //


// On se sert de socket IO pour emettre / diffuser des événéments
io.on('connection', (socket) => {

    // On garde l'id du user
    const userId = socket.id

    // Ici: renvoyer l'historique stocké dans mongo ?
    Msg.find().then(result => {
        socket.emit('output-messages', result)
    })

    // Sur l'action d'une nouvelle connextion d'un utilisateur
    socket.on('newUser', (username) => {

        // On le rajoute à la liste des utiliseurs
        users.push({ id: userId, username: username });

        // On prévient tout le monde de son arrivé
        io.emit('newNotification', username + ' vient de se connecter');

        // On demande à tous le monde mettre à jour la liste des utilisateurs
        io.emit('updateUsersList', users);
    });

    // Sur l'action d'un nouveau message reçu
    socket.on('newMessage', (username, msg, date) => {

        // Ici: stocker dans mongo ? 
        const message = new Msg({ username, msg, date });
        message.save().then(() => {
            io.emit('newMessage', username, msg, date);
        })

        // On diffuse le message à tout le monde
        // io.emit('newMessage', username, msg, date


    });

    // Sur l'action d'une déconnextion d'un utilisateur
    socket.on('disconnect', () => {

        // On cherche le user dans la liste pour le retirer
        users = users.filter(user => {
            return user.id != userId;
        });

        // On demande à tous le monde mettre à jour la liste des utilisateurs
        io.emit('updateUsersList', users);
    });
});

http.listen(PORT, () => {
    console.log('listening on http://localhost:' + PORT);
});